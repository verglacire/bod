# Initialiser le projet

## Initialiser les API

Entrer dans la partie backend 

```bash
cd api/bold
```

Créer un environement virtuel python (python3 doit déjà être installé sur la machine)

```bash
python3 -m venv .venv
```

Entrer dans l'environnement python (linux)

```bash
source .venv/bin/activate
```

Créer un fichier .env

```bash
touch.env
```
Remplir le fichier avec la variable SECRET_KEY (vous pourrez générer une secret key à cette addresse : https://django-secret-key-generator.netlify.app/)

```.env
SECRET_KEY="yoursecretkey"
```

Installer les requirements
```bash
pip install -r requirements.txt
```

Faire les migrations de Données
```bash
python manage.py migrate
```

Mettre en route les API
```bash
python manage.py runserver 0.0.0.0:8000
```

## Initialiser l'interface

Entrer dans a partie frontend

```bash
cd front/bold
```

Installer les requirements
````bash
npm i
````

Mettre en route le frontend
```bash
npm run start
```

Vous pouvez à présent accéder à l'applcation en allant sur l'url http://0.0.0.0:3000
